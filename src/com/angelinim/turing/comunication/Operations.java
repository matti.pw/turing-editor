package com.angelinim.turing.comunication;

public enum Operations {
    Login,
    Logout,
    Create,
    Share,
    Show,
    List,
    Edit,
    End_Edit
}
