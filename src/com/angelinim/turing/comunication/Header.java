package com.angelinim.turing.comunication;

import com.angelinim.turing.exceptions.InvalidJSON;
import org.json.simple.JSONObject;

import java.io.Serializable;
import java.net.Inet4Address;

public class Header implements Serializable {

    public Operations operations;
    public Inet4Address destIp;
    public String userToken;

    public static final String JSON_FIELD_NAME_OPERATION = "operation";
    public static final String JSON_FIELD_NAME_DEST_IP = "dest_ip";


    public static final String JSON_FIELD_NAME_USER_TOKEN = "user_token";

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();

        json.put(JSON_FIELD_NAME_OPERATION, operations);
        json.put(JSON_FIELD_NAME_DEST_IP, destIp);
        json.put(JSON_FIELD_NAME_USER_TOKEN, userToken);

        return json;
    }

    public String toJSONString() {
        return this.toJSON().toJSONString();
    }

    public static Header fromJSON(JSONObject json) throws InvalidJSON{
        try {
            Header header = new Header();

            header.operations = (Operations) json.get(JSON_FIELD_NAME_USER_TOKEN);
            header.destIp = (Inet4Address) Inet4Address.getByName((String) json.get(JSON_FIELD_NAME_DEST_IP));
            header.userToken = (String) json.get(JSON_FIELD_NAME_USER_TOKEN);

            return header;
        }catch (Exception e){
            throw new InvalidJSON(json);
        }

    }

}
