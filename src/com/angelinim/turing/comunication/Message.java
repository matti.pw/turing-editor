package com.angelinim.turing.comunication;

import com.angelinim.turing.exceptions.InvalidJSON;
import org.json.simple.JSONObject;
import java.io.Serializable;

public class Message implements Serializable {

    Header mHeader;
    JSONObject mBody;



    public static final String JSON_FIELD_NAME_HEADER = "header";
    public static final String JSON_FIELD_NAME_BODY = "body";


    public JSONObject toJSON() {
        JSONObject json = new JSONObject();

        json.put(JSON_FIELD_NAME_HEADER, mHeader.toJSON());
        json.put(JSON_FIELD_NAME_BODY, mBody);

        return json;
    }

    public String toJSONString() {
        return this.toJSON().toJSONString();
    }

    public static Message fromJSON(JSONObject json) throws InvalidJSON {
        try {
            Message message = new Message();

            JSONObject headerJSON = (JSONObject) json.get(JSON_FIELD_NAME_HEADER);
            message.mHeader = Header.fromJSON(headerJSON);
            message.mBody = (JSONObject) json.get(JSON_FIELD_NAME_BODY);

            return message;
        } catch (Exception e) {
            throw new InvalidJSON(json);
        }
    }
}
