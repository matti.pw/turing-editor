/*
 * This class is used to abstract a socket connection to the client,
 * and at the same time to menage this connection and retrieve information about the connection itself.
 * In second place I have put here the reading and writing logic about how understand that,
  * a message is fully read or written.
 * @author: A.Mattia (matti.pw@gmail.com)
 * @sudent_code: 502688
 * All the code was developed only from me.
 *
 */

package com.angelinim.turing.server.core;

import com.angelinim.turing.comunication.Message;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class ClientSocket {

    private static int MAX_RETRY = 25;

    public long id;

    public SocketChannel  socketChannel = null;

    public boolean endOfStreamReached = false;

    private byte[] mOutboundMessage;
    private int mOutboundMessageBytesWritten;

    private StringBuilder mInboundMessage = null;
    private boolean mInboundMessagereadyToProcess = false;
    private int mEmptyReadCountDown = MAX_RETRY;

    public ClientSocket(SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
        mOutboundMessageBytesWritten = 0;
    }


    /**
     * Read the available inputs bytes from the socket input stream,
     * more than one call may be needed to read the full message.
     * @param byteBuffer A preallocated buffer that the method can use to read from the socket.
     * @return A boolean value that tells if the inbound message can be processed.
     * @throws IOException
     */
    public boolean obtainInboundMessageFromSocket(ByteBuffer byteBuffer) throws IOException {
        if(this.mInboundMessagereadyToProcess){
            return true;
        }

        int bytesRead = this.read(byteBuffer);

        this.manageIncativityCounter(byteBuffer);

        if( byteBuffer.remaining() == 0 ) {
            return false;
        };

        if( bytesRead == -1 ){ //The client have close the stream
            //then the message is ready to process

            byteBuffer.clear(); // to be sure
            this.mInboundMessagereadyToProcess = true;
            return true;
        }

        //effective read
        byteBuffer.flip();
        this.flushReadBufferInTheInboundMessage(byteBuffer);
        byteBuffer.clear();

        return false;
    }

    /**
     * Write the current outbound message to the client socket, the mhetods write what the buffer enable it tho do,
     * more than one call may be needed to fully write it.
     * @param byteBuffer A prealocated buffet thath the method can use to write the message to the socket.
     * @return A boolean value stating if the outboundMessage is completely wried to the stream.
     * @throws IOException
     */
    public boolean writeOutboundMessage(ByteBuffer byteBuffer) throws IOException {
        if(mOutboundMessage.equals(null)){
            return false;
        }

        if(mOutboundMessage.length <= this.mOutboundMessageBytesWritten) {
            return true; // this call will return always true after that it  has written all the message.
        }

        byteBuffer.put(
            mOutboundMessage,
            mOutboundMessageBytesWritten,
            mOutboundMessage.length - mOutboundMessageBytesWritten
        );

        byteBuffer.flip();
        mOutboundMessageBytesWritten += this.socketChannel.write(byteBuffer);
        byteBuffer.clear();

        return false;
    }

    /**
     * Return all Full read Messages read from the readFromSocket Method
     * @return A list of all full read messages from the socket
     */
    public String getMessage() {
        return mInboundMessage.toString();
    }

    /**
     * This method add to the outbound queue of this socket the given message,
     * to be sent with the writeMessageFromQueue method.
     * @param m The Message to be write back in this socket
     */
    public void setOutBoundMessage(Message m){
        this.mOutboundMessage = m.toJSONString().getBytes();
        this.mOutboundMessageBytesWritten = 0;
    }

    /**
     * Returns if the socket have no more message to be write back,
     * so you can remove this socket from the selector check.
     * @preturn True if no more outbound message are present.
     */
    public boolean NoOutboundMessages() {
        return mOutboundMessage.equals(null);
    }

    /**
     * This method use the pre-alloced byteBuffer to Write all the possible bytes.
     * @param byteBuffer A pre-alloced ByteBuffer
     * @return Number of Bytes Written
     * @throws IOException
     */
    private int write(ByteBuffer byteBuffer) throws IOException{
        int bytesWritten      = this.socketChannel.write(byteBuffer);
        int totalBytesWritten = bytesWritten;

        while(bytesWritten > 0 && byteBuffer.hasRemaining()){
            bytesWritten = this.socketChannel.write(byteBuffer);
            totalBytesWritten += bytesWritten;
        }

        return totalBytesWritten;
    }

    /**
     * This method read all the possible readable bytes from the incoming channel of this socket.
     * @param byteBuffer A pre-alloced ByteBuffer
     * @return Number of Bytes read.
     * @throws IOException
     */
    private int read(ByteBuffer byteBuffer) throws IOException {
        int bytesRead = this.socketChannel.read(byteBuffer);
        int totalBytesRead = bytesRead;

        while(bytesRead > 0){
            bytesRead = this.socketChannel.read(byteBuffer);
            totalBytesRead += bytesRead;
        }
        if(bytesRead == -1){
            this.endOfStreamReached = true;
        }

        return totalBytesRead;
    }

    private boolean isInputSteamDead(){
        return this.mEmptyReadCountDown <= 0;
    }

    private void flushReadBufferInTheInboundMessage(ByteBuffer byteBuffer){
        while(byteBuffer.hasRemaining()) {
            mInboundMessage.append((char)byteBuffer.get());
        }
    }

    private boolean manageIncativityCounter(ByteBuffer byteBuffer){
        if( byteBuffer.remaining() == 0 ) { //Stream have no data to read
            mEmptyReadCountDown--;
            return true;
        }

        mEmptyReadCountDown = MAX_RETRY;
        return false;
    }


}

