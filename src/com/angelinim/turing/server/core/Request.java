package com.angelinim.turing.server.core;

import com.angelinim.turing.comunication.Message;

import java.nio.channels.SocketChannel;

public class Request{

    ClientSocket mClientSocket;
    Message mMessage;


    public Request(ClientSocket mClientSocket, Message mMessage) {
        this.mClientSocket = mClientSocket;
        this.mMessage = mMessage;
    }

    public ClientSocket getClientSocket() {
        return mClientSocket;
    }

    public void setClientSocket(ClientSocket mClientSocket) {
        this.mClientSocket = mClientSocket;
    }

    public Message getMessage() {
        return mMessage;
    }

    public void setMessage(Message mMessage) {
        this.mMessage = mMessage;
    }
}
