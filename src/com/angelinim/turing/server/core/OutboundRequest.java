/*
 * @author: A.Mattia (matti.pw@gmail.com)
 * @sudent_code: 502688
 * All the code was developed only from me.
 *
 */

package com.angelinim.turing.server.core;

import com.angelinim.turing.comunication.Message;
import java.nio.channels.SocketChannel;

public class OutboundRequest extends Request {

    Message mReplyMessage;

    public OutboundRequest(SocketChannel clientSocket) {
        super(clientSocket);
    }

    public void setReplyMessage(Message m){
        mReplyMessage = m;
    }

    public Message getReplyMessage(){
        return mReplyMessage;
    }

    static public OutboundRequest fromRequest(Request request, Message reply){
        OutboundRequest outRequest = (OutboundRequest) request;
        outRequest.setReplyMessage(reply);

        return  outRequest;
    }
}
