/*
 * @author: A.Mattia (matti.pw@gmail.com)
 * @sudent_code: 502688
 * All the code was developed only from me.
 *
 * This class will be used to avoid delay in accepting new incoming connections,
 * this will generate the new connection socket end forward it a NIOSocketProcessor,
 * in order to be handled.
 *
 */

package com.angelinim.turing.server.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class ConnectionProcessor implements Runnable{

    private Map<Long, ClientSocket> mSocketTable = new HashMap<Long, ClientSocket>();
    private Queue<ClientSocket> mConnectionQueue;
    private ServerSocketChannel mServerSocket;
    private int mTcpPort = 0;

    public ConnectionProcessor(int tcpPort,Queue<ClientSocket> connectionsQueue){
        mConnectionQueue = connectionsQueue;
        mTcpPort = tcpPort;
    }

    @Override
    public void run() {

        try{
            mServerSocket = ServerSocketChannel.open();
            mServerSocket.bind(new InetSocketAddress(mTcpPort));
        } catch(IOException e){
            e.printStackTrace();
            return;
        }


        while(true){
            try{
                SocketChannel socketChannel = mServerSocket.accept();

                //System.out.println("Socket accepted: " + socketChannel);

                this.mConnectionQueue.add(new ClientSocket(socketChannel));

            } catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
