/*
 * @author: A.Mattia (matti.pw@gmail.com)
 * @sudent_code: 502688
 * All the code was developed only from me.
 *
 * This is the core part of the server, it has the purpose to handle all the channels and sockets,
 * it follows a cycle to read from socket forwards the complete request to the requestProcessor, and reply
 * with the message in the outbound queue where the message processors will write.
 *
 */

package com.angelinim.turing.server.core;


import com.angelinim.turing.comunication.Message;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.*;

public class NIOSocketProcessor implements Runnable{

    private Queue<ClientSocket> mConnectionQueue; //should be as synchronized queue
    private Long mIncrementalSocketId;
    private Map<Long, ClientSocket> mSocketTable  = new HashMap<>();

    private Selector mReadSelector;
    private ByteBuffer mReadBuffer;
    private int mReadBufferSize = 1024;

    private Queue<OutboundRequest> mOutboundRequestQueue; //should be as synchronized queue
    private Selector mWriteSelector;
    private ByteBuffer mWriteBuffer;
    private int mWriteBufferSize = 1024;





    public NIOSocketProcessor (
            Queue<ClientSocket> mConnectionQueue,
            Queue<OutboundRequest> mOutboundRequestQueue
    ){
        this.mConnectionQueue = mConnectionQueue;
        this.mOutboundRequestQueue = mOutboundRequestQueue;
    }

    @Override
    public void run() {
        mReadBuffer = ByteBuffer.allocateDirect(mReadBufferSize);
        mWriteBuffer = ByteBuffer.allocateDirect(mWriteBufferSize);
        try{
            this.socketCycle();
        } catch (IOException e){
            System.err.println(e.getStackTrace().toString());
        }
    }

    private void socketCycle() throws IOException{
        this.setUpNewClientConnection();
        this.readChannels();
        this.writeOutboundRequests();
    }

    /**
     * This method take incoming new connection accepted from another thread,
     * and set them up to me checked for the channel selector for the ReadReady Event,
     * as well as setting up from the a ClientSocket instance to make easy handle them.
     * @throws IOException
     */
    private void setUpNewClientConnection() throws IOException {
        ClientSocket inboundSocket = mConnectionQueue.poll();

        while( !inboundSocket.equals(null) ){
            inboundSocket.id = mIncrementalSocketId++;
            inboundSocket.socketChannel.configureBlocking(false);

            mSocketTable.put(inboundSocket.id, inboundSocket);

            SelectionKey key = inboundSocket.socketChannel.register(mReadSelector, SelectionKey.OP_READ);
            key.attach(inboundSocket);

            inboundSocket = this.mConnectionQueue.poll();
        }
    }


    /**
     * Reads from each channel that the read selector mark as ready, the incoming messages.
     * @throws IOException
     */
    private void readChannels() throws IOException {
        int readyChannelsCount = mReadSelector.selectNow();

        if (readyChannelsCount <= 0) {
            return; //no channel to be readed
        }

        Set<SelectionKey> selectionKeys = mReadSelector.selectedKeys();
        Iterator<SelectionKey> keyIterator = selectionKeys.iterator();

        while(keyIterator.hasNext()) {
            SelectionKey key = keyIterator.next();

            readChannel(key);

            keyIterator.remove();
        }

        selectionKeys.clear();
    }

    /**
     * Read an incoming message from a ready to read channel,
     * and remove the channel form the ready to read list if this have reach the end of the stream.
     * @throws IOException
     */
    private void readChannel(SelectionKey channelKey) throws IOException{
        // Retrieve the pre-appended(see setUpNewConnection) channel socket that will be used for the read operation
        ClientSocket socket = (ClientSocket) channelKey.attachment();
        socket.read(mReadBuffer);


        socket.messageReader.read(socket, this.readByteBuffer);

        List<Message> fullMessages = socket.messageReader.getMessages();
        if(fullMessages.size() > 0){
            for(Message message : fullMessages){
                message.socketId = socket.socketId;
                this.messageProcessor.process(message, this.writeProxy);  //the message processor will eventually push outgoing messages into an IMessageWriter for this socket.
            }
            fullMessages.clear();
        }

        if(socket.endOfStreamReached){
            // the socket was closed
            System.out.println("Socket closed: " + socket.id);
            this.mSocketTable.remove(socket.id);
            channelKey.attach(null); //remove the socket from the key
            channelKey.cancel();
            channelKey.channel().close();
        }

    }

    private void writeOutboundRequests() throws IOException{

        /**
         * Here we must menage all the channel used to write back information,
         * so we have tho know all the channels we can write to and at the same time use the select,
         * to write only on ready channels.
         * This also mean that each client must have a dedicated outbound queue!!
         * This queue keeps tracks of the current written message, as well as the other message to send!
         */

        OutboundRequest reply= mOutboundRequestQueue.poll();

        while( !reply.equals(null) ){
            ClientSocket socket = reply.getClientSocket();

            socket.addOutBoundMessage(reply.getReplyMessage());
            socket.socketChannel.register(mWriteSelector, SelectionKey.OP_WRITE, socket);

            reply= mOutboundRequestQueue.poll();
        }

        // Select from the Selector.
        int writeReadyCount = this.mWriteSelector.selectNow();

        if(writeReadyCount > 0) {
            Set<SelectionKey> selectionKeys = this.mWriteSelector.selectedKeys();
            Iterator<SelectionKey> keyIterator = selectionKeys.iterator();

            while (keyIterator.hasNext()) {
                SelectionKey key = keyIterator.next();

                ClientSocket socket = (ClientSocket) key.attachment();

                if(!isAOpenSocket(socket)) {
                    socket.writeMessageFromQueue(mWriteBuffer);

                    if (socket.NoOutboundMessages()) {
                        key.cancel();
                    }
                } else {
                    key.cancel();
                }

                keyIterator.remove();
            }

            selectionKeys.clear();
        }

    }

    private boolean isAOpenSocket(ClientSocket socket) {
        return !socket.equals(null) && this.mSocketTable.containsKey(socket.id);
    }


    private void writeOutboundRequestToSocket(ClientSocket socket, OutboundRequest reply) throws IOException{



        int writeReady = mWriteSelector.selectNow();

        if(reply.equals(null) || socket.equals(null)) return;

        byte[] byteReply = reply.getReplyMessage().toJSONString().getBytes();

        for(int i=0; i<byteReply.length; i=i+mWriteBufferSize) {
            if(i+mWriteBufferSize <byteReply.length){
                mWriteBuffer.put(byteReply,i,i+mWriteBufferSize);
            }else{
                //The missing bytes to write are less than the buffer size
                mWriteBuffer.put(byteReply,i, i+mWriteBufferSize -byteReply.length);
            }
            socket.write(mWriteBuffer);
        }
/*


        if(writeReady > 0){
            Set<SelectionKey>      selectionKeys = this.writeSelector.selectedKeys();
            Iterator<SelectionKey> keyIterator   = selectionKeys.iterator();

            while(keyIterator.hasNext()){
                SelectionKey key = keyIterator.next();

                Socket socket = (Socket) key.attachment();

                socket.messageWriter.write(socket, this.writeByteBuffer);

                if(socket.messageWriter.isEmpty()){
                    this.nonEmptyToEmptySockets.add(socket);
                }

                keyIterator.remove();
            }

            selectionKeys.clear();
*/
        }
    }
}
